
import {useState, useEffect, useContext } from 'react';
import Hero from './../components/Banner';
import {Container, Form, Button, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import {Navigate, useParams} from 'react-router-dom';
import UserContext from '../UserContext';
import Footer from './../components/Footer';
const data = {
	title: 'Welcome to the Create Product Page',

};

export default function Update () {
	const { user } = useContext(UserContext);
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [active, setActive] = useState('');
	let token = localStorage.getItem('access');
	const [productInfo, setProductInfo] = useState({
			name: null,
			description: null,
			price: null
		});

		const {id} = useParams();

	useEffect(() => {
		if (name !== '' && description !== '' && price !== 0) {
			setActive(true);
		} else {
			setActive(false);
		}
		fetch(`https://thawing-sea-49056.herokuapp.com/products/${id}`).then(res => res.json()).then(convertedData =>{	
					setProductInfo({
						name: convertedData.name,
						description: convertedData.description,
						price: convertedData.price
					})
				});

	},[description, name, id, price])
	const updateProduct = async (event) => {
		event.preventDefault();

		const isUpdated = await fetch(`https://thawing-sea-49056.herokuapp.com/products/${id}`, {
					method: 'PUT',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},

			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		}).then(res => res.json()).then(data => {
			console.log(data)
			if (typeof data !== "undefined" && productInfo) {
				return true
			} else {
				return false
			}
		})
		if (isUpdated) {
			await Swal.fire({
				icon: 'success',
				title: ' Product Updated Successfully!'
			});
			setName('');
			setPrice(0);
			setDescription('');
			/*.push('/products')*/
		} else {
			Swal.fire({
				icon: 'error',
				title: 'Something Went Wrong',
				text: 'Contact IT Department!'
			})
		}
	
	};

	

	return(
		user.id && user.isAdmin
		?
		<>
			<Hero bannerData={data}/>
			<Row>
				<Col md={6} className="mr-auto ml-auto">
					<Container className="bgUpdate pb-1">
						<h1 className="text-center">Update Product Form</h1>

						<Form onSubmit={e => updateProduct(e)}>
						
							<Form.Group>
								<Form.Label>Name: </Form.Label>
								<Form.Control type="text" placeholder="Enter Product Name" value={name} onChange={event => setName(event.target.value)} required />
							</Form.Group>

						
							<Form.Group>
								<Form.Label>Description: </Form.Label>
								<Form.Control type="text" placeholder="Enter Description" value={description} onChange={event => setDescription(event.target.value)} required />
							</Form.Group>

							
							
							<Form.Group>
								<Form.Label>Price: </Form.Label>
								<Form.Control type="number" placeholder="Enter Price" value={price} onChange={event => setPrice(event.target.value)} required />
							</Form.Group>

							
							{
								active ?
									<Button variant="success" className="btn-block mt-5 mb-5" type="submit" to="/update">Update Information
									</Button>

								:
									<Button variant="success" className="btn-block mt-5 mb-5" disabled>Update Information
									</Button>
							}
							
						</Form>
					</Container>
					
				</Col>
			</Row>
			<Footer />
		</>
		:
		<Navigate to="/products" replace={true} />
		);

}