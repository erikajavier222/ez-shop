
export default function UpdateStatus (){

const deactivateProduct = (productId, isActive) => {

		fetch(`https://pacific-brook-27223.herokuapp.com/api/products/${productId}/archive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(result => result.json())
		.then(result => {
			if(result === true){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully archived!"
				})
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		});
	};

	const reactivateProduct = (productId, isActive) => {
		fetch(`https://pacific-brook-27223.herokuapp.com/api/products/${productId}/unarchive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(result => result.json())
		.then(result => {
			if(result === true){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully unarchived!"
				})
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		});
	};

	const deleteProduct = (productId) => {
			fetch(`https://pacific-brook-27223.herokuapp.com/api/products/${productId}/delete`, {
				method: "DELETE",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				}
			})
			.then(result => result.json())
			.then(result => {

				if(result === true){
					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Product successfully archived/unarchived"
					})
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again"
					})
				}
			})
		}
};
