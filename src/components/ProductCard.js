import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default function ProductCard({productProp}) {
	return(
	
		
		<Card className="productz p-2 m-4 d-md-inline-flex d-sm-inline-flex text-center" variant="success">
			<Card.Body>

				{<Card.Img variant="top" className="productimg" src={`/${productProp.name}.jpg`} />}
				<Card.Title>
					{productProp.name}
				</Card.Title>
				<Card.Text>
					{productProp.description}
				</Card.Text>
				<Card.Text>
					Price: ₱{productProp.price}
				</Card.Text>
				<Link to={`view/${productProp._id}`} className="btn btn-secondary badge badge-pill">
					View Product
				</Link>
			</Card.Body>
		</Card>
			
			
		);
}