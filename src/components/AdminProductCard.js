import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default function AdminProductCard({productProp}) {
	return(
	
		
		<Card className="productz m-4 d-md-inline-flex d-sm-inline-flex text-center" variant="success">
			<Card.Body>

				{<Card.Img variant="top" className="productimg" src={`/${productProp.name}.jpg`} />}
				<Card.Title>
					{productProp.name}
				</Card.Title>
				<Card.Text>
					{productProp.description}
				</Card.Text>
				<Card.Text>
					Price: ₱{productProp.price}
				</Card.Text>
				<Link to={`update/${productProp._id}`} className="btn btn-secondary badge badge-pill p-2 mr-1 mb-1">
					Update Information
				</Link>
				<Link to={`admin-view/${productProp._id}`} className="btn btn-secondary badge badge-pill p-2 ml-1 mb-1">
					Update Status
				</Link>
			</Card.Body>
		</Card>
			
			
		);
}